# Backend Developer Challenge

As a Backend developer we need more than writing code to create functionality. Focus should be on writing clean and maintainable code while keeping scalability in mind. Keep in mind you don't have to follow the instructions exactly, you can improvise/change/skip according to your convenience.

## Task 1 :: create a small rest API.

- Think of a product called `tour` with the following structure (not strictly but just for demonstration).

```
  id,
  title,
  url,
  description,
  location {
    name,
    url,
    latitude,
    longitude,
    city { name, url }
  },
  duration,
  is_live,
  is_promoted,
  seo { title, description, keywords },
  booking_count
```

- `[GET] /tours` :: Get list of tours with min details.
- `[GET] /tours/{id/url}` :: Get detailed view of a tour with given id or slug/url.
- `[POST] /products` :: Create a new product.
- `[PUT] /products` :: Edit an existing product.
- `[DELETE] /products/{id}` :: Delete a product with given id.

## Task 2 :: Testing and Validation

- Test the functionality and writing working tests.
- Validate data coming in for post/put requests with proper request validation.
- Response should have proper response codes and error codes (in case of errors).

## Task 3 :: Containerization and deployment

- Create a development ready dockerized environment.
- Create a production ready dockerized environment.
- Don't hesitate to write convenience script for booting up the project and/or deploying.

## Preferred tech stack and considerations

- PHP/Laravel for writing the api and tests
- Mysql for the database
- Docker for containerization
- Faker for generating fake data (at least 100 records), and create seeders to seed the database.
- Would be better to create 2 different docker containers, one that runs the database and the other runs the api.

## Lastly

Document the project, how to spin up the project, how to run the tests and how to generate the database and the dummy content.
After you have the documentation, create a repository in GitLab/Github/Bitbucket and share with us.
